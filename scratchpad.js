let cats = [
  {
    id: 1,
    name: "Tuna",
    breed: "Siamese",
  },
  {
    id: 2,
    name: "Chester",
    breed: "Tabby",
  },
  { id: 3, name: "Blue", breed: "Naked" },
];

// let newCat = { id: 4, name: "Sally", breed: "Naked" };

console.log(cats);

let catObject = cats.find((catObj) => catObj.id === 1);
console.log(catObject);

// let newUpdatedCat = {
//     id: 2,
//     name: "Cheeto",
//     breed: "Tabby",
// }


let index = cats.findIndex((cat) => cat.id === 1);
console.log(index);
// cats.splice(index, 1, newUpdatedCat);

// console.log(cats);
// console.log(cats[index]);

// cats.push(newCat);
// console.log(cats);

// let index = cats.findIndex((cat) => cat.id === 2);
// console.log(index);
// cats.splice(index, 1);
// console.log(cats);
